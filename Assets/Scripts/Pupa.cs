﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;


public class Pupa : MonoBehaviour
{
    private const float Duration = 0.2f;
    private const float DurationFly = 1.0f;
    private const float Thrust = 8.0f;
    private const float optimalRatioPlatform = 1.7f;
    private const float optimalRatioPupa = 1.6f;

    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private SpriteRenderer spriteRenderer;

    [SerializeField] private Sprite pupaSprite;
    [SerializeField] private Sprite pupaDieSprite;

    private Collider2D currentTriggerCollider;

    private Vector3 bounds;

    private PlatformsType platformsType;

    private float distanceBetweenPlatforms;
    private float quarterWidhtScreen;

    private int hp = 3;
    private int startPositionMouse;
    private int endPositionMouse;

    private bool isMove = false;
    private bool isJamp = false;

    public void CalculateBounds(Vector2 bounds)
    {
        this.bounds = bounds;
        distanceBetweenPlatforms = bounds.y / optimalRatioPlatform;
        quarterWidhtScreen = bounds.x / 2;
    }

    public void StartPosition(float platformMediumPositionY)
    {
        spriteRenderer.sprite = pupaSprite;

        platformsType = PlatformsType.Medium;
        isMove = false;

        float boundsX = quarterWidhtScreen * 2.0f;
        transform.position = new Vector3(-boundsX + quarterWidhtScreen, platformMediumPositionY);
        
        Bounds boundsSize = spriteRenderer.bounds;
        transform.position += new Vector3(boundsSize.size.x, boundsSize.size.y / optimalRatioPupa);
    }

    public void Static(bool isStatic)
    {
        if (isStatic)
        {
            rigidbody.bodyType = RigidbodyType2D.Static;
        }
        else
        {
            rigidbody.bodyType = RigidbodyType2D.Dynamic;
        }

    }

    public void RecoveryHP()
    {
        hp = 3;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isJamp)
        {
            isMove = false;
            isJamp = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.TryGetComponent(out Obstacles obstacle))
        {
            if (currentTriggerCollider == null || currentTriggerCollider != other)
            {
                currentTriggerCollider = other;
                CheckObstacles(obstacle);
            }
        }
        else
        {
            currentTriggerCollider = null;
        }
    }
   
    private void CheckObstacles(Obstacles obj)
    {
        switch (obj.ObstaclesType)
        {
            case ObstaclesType.Apple:
                if (hp < 3)
                {
                    hp++;
                    GameManager.Instance.AddHitPoint(hp);
                }
                Destroy(obj.gameObject);
                break;

            case ObstaclesType.Cactus:
            case ObstaclesType.Dirt:
            case ObstaclesType.Stone:
                ChangeHP();
                break;
        }
    }

    private void ChangeHP()
    {
        if (hp > 0)
        {
            hp--;
            GameManager.Instance.RemoveHitPoint(hp);

            if (hp == 0)
            {
                Dead();
            }
        }
    }

    private void Dead()
    {
        GameManager.Instance.StopObstacles();
        
        spriteRenderer.sprite = pupaDieSprite;

        float overBorderScreen = bounds.y + 1.0f;
        transform.DOMoveY(overBorderScreen, DurationFly).OnComplete(() =>
            GameManager.Instance.Lose());
    }
    
    
    private void Move(float targetY)
    {
        isMove = true;

        transform.DOMoveY(targetY, Duration).OnComplete(() =>
            isMove = false);
    }

    void Update()
    {
        if (GameManager.Instance.GameState == GameState.Play)
        {
            if (!isMove)
            {
                if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
                {
                    Vector3 startPosition = Camera.main.WorldToScreenPoint(Input.mousePosition);
                    startPositionMouse = Convert.ToInt32(startPosition.y);
                }

                if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
                {
                    Vector3 endPosition = Camera.main.WorldToScreenPoint(Input.mousePosition);
                    endPositionMouse = Convert.ToInt32(endPosition.y);

                    if (endPositionMouse > startPositionMouse)
                    {
                        if (platformsType != PlatformsType.Upper)
                        {
                            platformsType++;
                            Move(transform.position.y + distanceBetweenPlatforms);
                            
                        }
                    }

                    if (endPositionMouse < startPositionMouse)
                    {
                        if (platformsType != PlatformsType.Lower)
                        {
                            platformsType--;
                            Move(transform.position.y - distanceBetweenPlatforms);
                        }
                    }

                    if (endPositionMouse == startPositionMouse)
                    {
                        isMove = true;
                        isJamp = true;
                        rigidbody.AddForce(Vector2.up * Thrust, ForceMode2D.Impulse);
                    }
                }
            }
        }
    }
}
