﻿using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

public class SpawnerObstacles : MonoBehaviour
{
    private const float Duration = 3.0f;
    private const float optimalRatioPlatform = 1.7f;

    [SerializeField] private Camera camera;
    [SerializeField] private Obstacles[] obstaclesType = new Obstacles [4];

    private List<Obstacles> obstacles = new List<Obstacles>(); 
    
    private float waitingUpperOstacles = 1.0f;
    private float waitingMediumOstacles = 1.0f;
    private float waitingLowerOstacles = 1.0f;
    private float distanceBetweenPlatforms;

    private int exclusionNubmer = 10;

    private PlatformsType platformsType;

    private Vector2 bounds;

    private void Start()
    {
        bounds = camera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        distanceBetweenPlatforms = bounds.y / optimalRatioPlatform;
    }

    public void StopObstacles()
    {
        DOTween.PauseAll();
    }

    public void PlayObstacles()
    {
        DOTween.PlayAll();
    }

    public void DeleteAllObstacles()
    {
        for (int i = obstacles.Count; i > 0; i--)
        {
            DeleteObstacle(obstacles[i - 1]);
        }
    }

    private void MoveObstacles(Obstacles obj)
    {
        obj.transform.DOMoveX(-bounds.x - 5, Duration).OnComplete(() =>
            DeleteObstacle(obj));
    }

    private void DeleteObstacle(Obstacles obj)
    {
        if (obj != null)
        {
            obstacles.Remove(obj);
            Destroy(obj.gameObject);
        }
    }


    private float SpawnObstacles()
    {
        Obstacles obj = ParametersObstacle();

        switch(platformsType)
        {
            case PlatformsType.Upper:
                obj.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y + distanceBetweenPlatforms);
                break;
            case PlatformsType.Lower:
                obj.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y - distanceBetweenPlatforms);
                break;
        }

        float waitingOstacles = CalculateObstacleFrequency();
        return waitingOstacles;
    }

    private float CalculateObstacleFrequency()
    {
        float obstacleFrequency = UnityEngine.Random.Range(1, 4);
        return obstacleFrequency;
    }

    private Obstacles ParametersObstacle()
    {
        int num = CalculateNumberObstaclesType();

        Obstacles obstacle = Instantiate(obstaclesType[num], transform);

        if (obstacle.ObstaclesType != ObstaclesType.Dirt)
        {
            obstacle.transform.position += new Vector3(obstacle.BoundsSize.size.x / 2, obstacle.BoundsSize.size.y / 2);
        }
        MoveObstacles(obstacle);
        obstacles.Add(obstacle);
        return obstacle;
    }

    private int CalculateNumberObstaclesType()
    {
        int num = UnityEngine.Random.Range(0, 4);

        if (num == exclusionNubmer)
        {
            CalculateNumberObstaclesType();
            return num = 0;
        }
        else
        {
            exclusionNubmer = num;
        }

        return num;
    }
    
    private void Update()
    {
        if (GameManager.Instance.GameState == GameState.Play)
        {
            waitingUpperOstacles -= Time.deltaTime;
            waitingMediumOstacles -= Time.deltaTime;
            waitingLowerOstacles -= Time.deltaTime;

            if (waitingUpperOstacles <= 0)
            {
                platformsType = PlatformsType.Upper;
                waitingUpperOstacles = SpawnObstacles();
            }

            if (waitingMediumOstacles <= 0)
            {
                platformsType = PlatformsType.Medium;
                waitingMediumOstacles = SpawnObstacles();
            }
               
            if(waitingLowerOstacles <= 0)
            {
                platformsType = PlatformsType.Lower;
                waitingLowerOstacles = SpawnObstacles();
            }
        }
    }
}
