﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;
    public Bounds BoundsSize => spriteRenderer.bounds;


    private PlatformsType platformsType = PlatformsType.None;
    
    public PlatformsType PlatformsType
    {
        get
        {
            return platformsType;
        }
        set
        {
            platformsType = value;
        }
    }
}
