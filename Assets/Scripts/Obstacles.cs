﻿using UnityEngine;

public class Obstacles : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;
    public Bounds BoundsSize => spriteRenderer.bounds;


    [SerializeField] ObstaclesType obstaclesType;
    public ObstaclesType ObstaclesType => obstaclesType;
}
