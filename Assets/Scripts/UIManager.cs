﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Text scoreText;
    [SerializeField] private Text endScoreText;
    [SerializeField] private Text recordText;
    [SerializeField] private Image gameOverMenu;
    [SerializeField] private Image pauseMenu;
    [SerializeField] private Button startButton;
    [SerializeField] private Button pauseButton;
    [SerializeField] private Button continueButton;
    [SerializeField] private Button restartPauseButton;
    [SerializeField] private Button restartGameOverButton;
    
    [SerializeField] private Image[] hitPoints = new Image[3];

    private void OnEnable()
    {
        startButton.onClick.AddListener(StartGame);
        pauseButton.onClick.AddListener(Pause);
        continueButton.onClick.AddListener(Continue);
        restartPauseButton.onClick.AddListener(Restart);
        restartGameOverButton.onClick.AddListener(Restart);
    }

    private void OnDisable()
    {
        startButton.onClick.RemoveListener(StartGame);
        pauseButton.onClick.RemoveListener(Pause);
        continueButton.onClick.RemoveListener(Continue);
        restartPauseButton.onClick.RemoveListener(Restart);
        restartGameOverButton.onClick.RemoveListener(Restart);
    }

    public void RemoveHitPoint(int hp)
    {
        hitPoints[hp].gameObject.SetActive(false);
    }

    public void AddHitPoint(int hp)
    {
        if (hp <= 3)
        {
            hitPoints[hp - 1].gameObject.SetActive(true);
        }
    }

    public void ChangeScore(float score)
    {
        scoreText.text = string.Format("Score: {0}", Convert.ToInt32(score));
    }

    public void Losing(float score, float record)
    {
        gameOverMenu.gameObject.SetActive(true);
        
        endScoreText.text = string.Format("Score: {0}", Convert.ToInt32(score));
        recordText.text = string.Format("Record: {0}", Convert.ToInt32(record));
    }
    
    private void StartGame()
    {
        startButton.gameObject.SetActive(false);
        
        scoreText.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(true);

        for (int i = 0; i < hitPoints.Length; i++)
        {
            hitPoints[i].gameObject.SetActive(true);
        }
        
        GameManager.Instance.StartGame();
    }
    private void Pause()
    {
        GameManager.Instance.Pause();
        pauseMenu.gameObject.SetActive(true);
    }

    private void Restart()
    {
        for (int i = 0; i < hitPoints.Length; i++)
        {
            hitPoints[i].gameObject.SetActive(true);
        }

        pauseMenu.gameObject.SetActive(false);
        gameOverMenu.gameObject.SetActive(false);
        
        GameManager.Instance.Restart();
    }

    private void Continue()
    {
        GameManager.Instance.Continue();
        pauseMenu.gameObject.SetActive(false);
    }

}
