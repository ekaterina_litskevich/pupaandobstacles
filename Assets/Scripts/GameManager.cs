﻿using UnityEngine;

public class GameManager : MonoBehaviourSingleton<GameManager>
{
    private const float optimalRatioPlatform = 1.7f;
    private const float optimalPartPlatform = 2.3f;

    private const string KeyRecord = "record";

    [SerializeField] private UIManager uiManager; 
    [SerializeField] private Camera camera;
    [SerializeField] private SpawnerObstacles spawnerObstacles;
    [SerializeField] private Platform platformPrefab = null;
    [SerializeField] private Pupa pupaPrefab = null;

    private Pupa pupa;

    private float score = 0;
    private float record = 0;
    private float platformMediumPositionY;

    private GameState gameState;
    public GameState GameState
    {
        get
        {
            return gameState;
        }
        set
        {
            gameState = value;
        }
    }

    protected override void SingletonStarted()
    {
        GameState = GameState.Start;
    }
    
    public void StartGame()
    {
        GameState = GameState.Play;
        
        Vector2 bounds = camera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        
        CreatePlatform(bounds);
        CreatePupa(bounds);
    }

    public void Pause()
    {
        GameState = GameState.Pause;
        spawnerObstacles.StopObstacles();
        pupa.Static(true);
    }

    public void Continue()
    {
        GameState = GameState.Play;
        spawnerObstacles.PlayObstacles();
        pupa.Static(false);
    }

    public void Restart()
    {
        GameState = GameState.Play;
        score = 0;
        spawnerObstacles.DeleteAllObstacles();
        pupa.StartPosition(platformMediumPositionY);
        pupa.RecoveryHP();
        pupa.Static(false);
    }

    public void StopObstacles()
    {
        GameState = GameState.Lose;
        spawnerObstacles.StopObstacles();
    }
    public void Lose()
    {
        GameState = GameState.Lose;
        pupa.Static(true);

        record = PlayerPrefs.GetFloat(KeyRecord, record);

        if (score > record)
        {
            record = score;
            PlayerPrefs.SetFloat(KeyRecord, record);
            PlayerPrefs.Save();
        }
       
        uiManager.Losing(score, record);
    }


    public void AddHitPoint(int hp)
    {
        uiManager.AddHitPoint(hp);
    }

    public void RemoveHitPoint(int hp)
    {
        uiManager.RemoveHitPoint(hp);
    }

    private void CreatePlatform(Vector2 bounds)
    {
        float distanceBetweenPlatforms = bounds.y / optimalRatioPlatform;

        GameObject platforms = new GameObject("Platforms");

        for (int i = 0; i < 3; i++)
        {
            Platform platform = Instantiate(platformPrefab, transform);
            float optimalRatio = platform.BoundsSize.size.y / optimalPartPlatform;
            platform.transform.position = new Vector2(0, -bounds.y + optimalRatio + (distanceBetweenPlatforms * i));
            platform.PlatformsType = (PlatformsType)i + 1;
            platform.transform.SetParent(platforms.transform);

            if (i == 1)
            {
                platformMediumPositionY = platform.transform.position.y;
            }
        }
    }

    private void CreatePupa(Vector2 bounds)
    {
        pupa = Instantiate(pupaPrefab, transform);
        pupa.CalculateBounds(bounds);
        pupa.StartPosition(platformMediumPositionY);
    }

    private void ChangeScore()
    {
        uiManager.ChangeScore(score);
    }

    void Update()
    {
        if (GameState == GameState.Play)
        {
            score += Time.deltaTime;
            ChangeScore();
        }
    }
}
