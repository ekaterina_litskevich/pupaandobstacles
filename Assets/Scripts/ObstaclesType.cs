﻿public enum ObstaclesType
{
    None = 0,
    Apple = 1,
    Cactus = 2,
    Dirt = 3,
    Stone = 4
}
